from pyb import UART
import utime as time
from pyb import SD
from uos import VfsFat

encoding = 'utf-8'

uart = UART(4, 593)                         # init with given baudrate
uart.init(593, bits=8, parity=None, stop=1) #9600 baud

 
vfs = VfsFat(SD)
logfile_NMEA = vfs.open('NMEA_Matek.txt','a') 
logfile_NMEA.write('\n\n\n#NMEA from MATEK QMC5883\n\n')
 


counter = 0
print("starting")

while (1):
     if (uart.any()) :        
          temp2 = uart.read()
          temp3 = str(temp2, encoding)
          print(temp3)
          logfile_NMEA.write(temp3+'\n')
          counter =+ counter
     logfile_NMEA.flush()      



